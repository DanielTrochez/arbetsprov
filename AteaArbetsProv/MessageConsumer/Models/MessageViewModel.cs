﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MessageConsumer.MessageReference;

namespace MessageConsumer.Models
{
    public class MessageViewModel
    {

        public List<MessageModel> MessageList { get; set; }
    }
}