﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Proxies;
using System.Security.AccessControl;
using System.Web;
using System.Web.Mvc;
using MessageConsumer.MessageReference;
using MessageConsumer.Models;

namespace MessageConsumer.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            var model = new MessageViewModel();
            model.MessageList = GetModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(string startDate, string endDate)
        {
            var model = new MessageViewModel();

            if (startDate != string.Empty && endDate != string.Empty)
            {
                ModelState.Clear();
                var proxy = new MessageServiceClient();
                var convertedStart = Convert.ToDateTime(startDate);
                var convertedEnd = Convert.ToDateTime(endDate);

                var sortedList = proxy.GetMessagesByDate(convertedStart, convertedEnd);
                model.MessageList = sortedList.OrderByDescending(x => x.TimeStamp).ToList();
                return View(model);
            }

            model.MessageList = GetModel();
            return View(model);


        }

        public List<MessageModel> GetModel()
        {
            var proxy = new MessageServiceClient();
            var model = proxy.GetAllMessages().OrderByDescending(x => x.TimeStamp).ToList();

            return model;
        }
    }
}