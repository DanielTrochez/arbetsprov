﻿using System;
using System.Data;
using MessageSender.MessageReference;

namespace MessageSender
{
    class Program
    {
       
        static void Main()
        {
            string content;
            var title = Init(out content);
            SendMessage(title, content);
        }

        private static string Init(out string content)
        {
            Console.Clear();
            Console.WriteLine("Hello!");
            Console.WriteLine("Please type in the title for your message.");
            var title = Console.ReadLine();
            Console.WriteLine("Now write your message.");
            content = Console.ReadLine();
            return title;
        }

        private static void SendMessage(string title, string content)
        {
            if (title != string.Empty && content != string.Empty)
            {
                var proxy = new MessageServiceClient();

                var messageToSend = new MessageModel { Title = title, Content = content, TimeStamp = DateTime.Now };

                proxy.SendMessage(messageToSend);

                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Your message has been sent!");
                Console.ReadLine();
            }
            else
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Your inputs where empty... press any key to try again.");
                Console.ReadLine();
                Main();
            }

        }
    }
}
