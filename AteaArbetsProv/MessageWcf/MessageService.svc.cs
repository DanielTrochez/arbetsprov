﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using MessageWcf.DatabaseModels;

namespace MessageWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IMessageService
    {

        public void SendMessage(MessageModel message)
        {
            using (var context = new MessageEntities())
            {
                context.Messages.Add(new Message
                {
                    Subject = message.Title,
                    Message1 = message.Content,
                    TimeStamp = message.TimeStamp
                });
                context.SaveChanges();
            }
        }

        public List<MessageModel> GetAllMessages()
        {
            var allMessages = new List<MessageModel>();
            using (var context = new MessageEntities())
            {
                allMessages = context.Messages.Select(x => new MessageModel{Title = x.Subject, Content = x.Message1, TimeStamp = x.TimeStamp}).ToList();
            }
            
            return allMessages;
        }

        public List<MessageModel> GetMessagesByDate(DateTime startDate, DateTime endDate)
        {
            var messages = new List<MessageModel>();
            using (var context = new MessageEntities())
            {
                endDate = endDate.AddDays(1);
                messages = context.Messages.Where(x => x.TimeStamp >= startDate.Date && x.TimeStamp <= endDate.Date)
                    .Select(x => new MessageModel { Title = x.Subject, Content = x.Message1, TimeStamp = x.TimeStamp }).ToList();
            }
            return messages;
        }
    }
}
