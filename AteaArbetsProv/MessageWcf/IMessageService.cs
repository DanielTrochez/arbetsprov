﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MessageWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IMessageService
    {
       [OperationContract]
        void SendMessage(MessageModel message);

        [OperationContract]
        List<MessageModel> GetAllMessages();

        [OperationContract]
        List<MessageModel> GetMessagesByDate(DateTime startDate, DateTime endDate);
    }


  
}
